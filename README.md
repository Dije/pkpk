## PT. Perdana Karya Perkasa, Tbk (PKPK)
---

**PKPK** is a website using a responsive Bootstrap v4.4.1. Combine with [adminLTE3](https://adminlte.io/themes/v3/) as the admin page.

---

## Changelog
|DATE | VERSION | MOMENT |
|:------:|:------:|:------|
| 15/12/2021 | V1.0.0 | PKPK initial looks | 
| 16/12/2021 | V2.0.0 | The New PKPK Website| 
| 16/12/2021 | V2.0.1 | 1. Adjusting unimportant files and folders<br>2. Convert some hardcode into php echo database |
| 20/12/2021 | V2.0.2 | Finishing adjust files using echo database |
| 24/12/2021 | V2.0.3 | 1. Importing Pictures<br>2. Adjusting slideshow picture<br>3. Implementing a new Logo of PKPK |
| 20/07/2022 | V2.1.0 | 1. Changing Graphics into Table on Company Report and Laporan Perusahaan<br>2. Changing PKPK Location on Contact and Hubungi Kami|

## Deployment  
[pkpk-tbk.co.id](https://pkpk-tbk.co.id) 