	<!-- Header -->
	<?php $title = "SOCIAL | PKPK";
	$page = "sosial"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../Controller/homeController.php');
	$companyprofile = new companyprofileController();
	$home = new homeController();
	$CPdata = $companyprofile->getData();
	$indx = $home->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar --> 

		 <!-- Hero Section Begin -->  
		<section class="cta-section spad set-bg opacity" data-setbg="admin/assets/img/esg/social-header.png">
			<div class="text">
				<div class="cta-text">
					<h2>Social</h2> 
					<br>
					<div class="text-sub"> <p style="white-space: pre-line;">Empowering social communities and matters
is a part of PKPK’s mission to realize the ESG social pillar.</p> 
					</div>  
				</div>   
			</div>   
		</section> 
		<!-- Hero Section End --> 

		<!-- NEWS Section Begin -->
		<section class="contact-section spad2">
			<div class="container">
				<div class="contact-text">
					<h3><b>What we do to make it happen</b></h3>  
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							PKPK is aware that social communities play an important role in realizing a sustainable future. Therefore, society needs to be embraced and prosperoused. PKPK continues to intensify its efforts to create employment opportunities and build facilities that support the lives of the surrounding community.</p> 
						</div>
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							PKPK also prioritizes employee safety and welfare. We always ensure that all employees receive the best work protection. Apart from that, our employees will be ensured to get all the benefits that being offered by the company.</p> 
						</div>
					</div>  
					<div class="container">
						<div class="row">
							<!-- Hero Section Begin -->
								<section class="hero-section carouseelll">
									<div class="hs-slider owl-carousel normall"> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-2.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-3.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-4.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-5.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-6.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-7.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-8.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-9.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/social/social-10.jpeg">
										</div> 
									</div>
								</section>
							<!-- Hero Section End -->
						</div>
					</div>
				</div> 
			</div>
		</section>
		<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
