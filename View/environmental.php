	<!-- Header -->
	<?php $title = "ENVIRONMENTAL | PKPK";
	$page = "lingkungan"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../Controller/homeController.php');
	$companyprofile = new companyprofileController();
	$home = new homeController();
	$CPdata = $companyprofile->getData();
	$indx = $home->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar --> 

		 <!-- Hero Section Begin --> 
		<section class="cta-section spad set-bg opacity" data-setbg="admin/assets/img/esg/environmental-header.png">
			<div class="text">
				<div class="cta-text">
					<h2>Environment</h2> 
					<br>
					<div class="text-sub"> <p style="white-space: pre-line;">Focusing on energy sector, PKPK 
						is committed to create a sustainable future.</p> 
					</div>  
				</div>   
			</div>   
		</section>
		<!-- Hero Section End --> 

		<!-- NEWS Section Begin -->
		<section class="contact-section spad2">
			<div class="container"> 
				<div class="contact-text">
					<h3><b>What we do to make it happen</b></h3>  
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							Focusing on building energy sector business, PKPK realizes the importance of awareness to keep the environment clean and green. Therefore, PKPK do several strategies such as reducing the use of energy electricity, fuel, water, and material for its daily operational. </p> 
						</div>
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							Coupled with increasingly global warming issue like climate change, PKPK is committed to carry out massive deforestation, and developing optimal technology to reduce carbon emissions for the better world.</p> 
						</div>
					</div>  
					<div class="container">
						<div class="row">
							<!-- Hero Section Begin -->
								<section class="hero-section carouseelll">
									<div class="hs-slider owl-carousel normall"> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/environment/environment-1.png">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/environment/environment-2.jpg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/environment/environment-3.jpg">
										</div> 
									</div>
								</section>
							<!-- Hero Section End -->
						</div>
					</div>
				</div> 
			</div>
		</section>
		<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
