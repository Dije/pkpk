	<!-- Header -->
	<?php $title = "Company Profile | PKPK";
	$page = "tentang-kami"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../Controller/companyprofileController.php'); 
	$companyprofile = new companyprofileController();
	$CPdata = $companyprofile->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar -->

		<!-- Subheader -->
		<?php include 'include/subheader.php' ?>
		<!-- Subheader -->

		<section class="contact-section spad2">
			<div class="container">
				<div class="row"> 
					<div class="contact-text"> 
						<?php for($i=0; $i< count($CPdata); $i++){ ?>
							<p class="bodytext2"><?php echo $CPdata[$i]['body_eng']; ?></p> 		
						<?php } ?> 
					</div> 
				</div>
			</div>
		</section>

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
