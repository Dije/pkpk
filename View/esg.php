	<!-- Header -->
	<?php $title = "ESG | PKPK";
	$page = "esg"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../Controller/companyprofileController.php'); 
	$companyprofile = new companyprofileController();
	$CPdata = $companyprofile->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar -->  

		<!-- Hero Section Begin --> 
		<section class="cta-section spad set-bg opacity" data-setbg="admin/assets/img/esg/header.png">
			<div class="text">
				<div class="cta-text">
					<h2>Environment, 
						Social,
						& Governance</h2> 
					<br>
					<div class="text-sub">
						<p>The realization of 3 ESG's pillars is our long-term commitment to reach a sustainable future.</p> 
					</div>  
				</div>   
			</div>  
		</section>
		<!-- Hero Section End -->
 
		<!-- NEWS Section Begin -->
	<section class="contact-section spad2">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<div class="contact-text">
						<h3><b>Our Mission on ESG</b></h3>  
						<p style="white-space: pre-line; text-align: justify;">
						PKPK firmly stick to the fact that environmental preservation, contribution to social life for the welfare of society, and transparent corporate governance are needed to create a sustainable future.</p> 
					</div>
				</div>  
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<div class="contact-text esgcontainer">
								<img class="mx-auto esg" src="admin/assets/img/esg/environmental.png"> 
								<div class="bottom-left">
									<img class="mx-auto esg-icon" src="admin/assets/icon/esg/environment-icon.png"> 
									<h6>ENVIRONMENTAL</h6>   
									<p class="esgheight">
									PKPK commits to preserve the environment for a sustainable future.</p>
									<p class="see-more"><a href="./environmental">See More</a></p>
								</div>  
							</div>
						</div>  
						<div class="col-lg-4 col-md-4 pad20">
							<div class="contact-text esgcontainer">
								<img class="mx-auto esg" src="admin/assets/img/esg/social.png"> 
								<div class="bottom-left"> 
									<img class="mx-auto esg-icon" src="admin/assets/icon/esg/social-icon.png"> 
									<h6>SOCIAL</h6>   
									<p class="esgheight">
									Empowering social communities and our employees is a part of PKPK mission to realize the ESG social pillar.</p>
									<p class="see-more"><a href="./social">See More</a></p>
								</div>   
							</div>
						</div>  
						<div class="col-lg-4 col-md-4 pad20">
							<div class="contact-text esgcontainer">
								<img class="mx-auto esg" src="admin/assets/img/esg/governance.png"> 
								<div class="bottom-left"> 
									<img class="mx-auto esg-icon" src="admin/assets/icon/esg/governance-icon.png"> 
									<h6>GOVERNANCE</h6>  
									<p class="esgheight">
									We believe that to bulid a transparent, fair, and responsible corporate should be in harmony with GCG (Good Corporate Governance) aspects.</p>
									<p class="see-more"><a href="./governance">See More</a></p>
								</div>  
							</div>
						</div>  
					</div>
				</div>
			</div> 
		</div>
	</section>
	<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
