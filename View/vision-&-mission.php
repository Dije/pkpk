<!-- Header -->
<?php $title = "Vision & Mission | PKPK";
$page = "visi-&-misi"; 
include 'include/header.php'; 
include_once (PROJECT_ROOT_PATH . '/../Controller/vissionmissionController.php');
$vissionmission = new vissionmissionController(); 
$VMdata = $vissionmission->getData(); 
?>
<!-- Header --> 

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Subheader -->
	<?php include 'include/subheader.php' ?>
	<!-- Subheader -->
 
	<section class="contact-section spad2">
		<div class="container">
			<div class="row">     
					<div class="col-lg-3 col-md-3" style="display: flex;"> 
						<div class="pad20 background"> 
							<div class="image">
								<img class="mx-auto sign" src="admin/assets/img/vision&misison/vision.png"> 
							</div>
							<div class="contact-text"> 
							<div class="ct-item"> 
								<div class="ct-text">
									<h5 style="white-space: pre-line; text-align: center;">VISION</h5>
									<p class="bodytext">
										<?php for($j=0; $j< count($VMdata); $j++){ ?>
											"<?php echo $VMdata[$j]['vission']; ?>"
										<?php } ?> 
									</p> 	
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3" style="display: flex;"> 
						<div class="pad20 background"> 
							<div class="image">
								<img class="mx-auto sign" src="admin/assets/img/vision&misison/mission.png">
							</div>
							<div class="contact-text"> 
							<div class="ct-item"> 
								<div class="ct-text">
									<h5 style="white-space: pre-line; text-align: center;">MISSION</h5>
									<p class="bodytext">
										<?php for($j=0; $j< count($VMdata); $j++){ ?>
											"<?php echo $VMdata[$j]['mission']; ?>"
										<?php } ?> 
									</p> 	
								</div>
							</div>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-md-3" style="display: flex;"> 
						<div class="pad20 background"> 
							<div class="image">
								<img class="mx-auto sign" src="admin/assets/img/vision&misison/motto.png">
							</div>
							<div class="contact-text"> 
							<div class="ct-item"> 
								<div class="ct-text">
									<h5 style="white-space: pre-line; text-align: center;">MOTTO</h5>
									<p class="bodytext">
										<?php for($j=0; $j< count($VMdata); $j++){ ?>
											"<?php echo $VMdata[$j]['motto']; ?>"
										<?php } ?> 
									</p> 	
								</div>
							</div>
							</div>
						</div>
					</div> 
					<div class="col-lg-3 col-md-3" style="display: flex;"> 
						<div class="pad20 background"> 
							<div class="image">
								<img class="mx-auto sign" src="admin/assets/img/vision&misison/phylosphy.png">
							</div>
							<div class="contact-text"> 
							<div class="ct-item"> 
								<div class="ct-text">
									<h5 style="white-space: pre-line; text-align: center;">PHILOSOPHY</h5>
									<p class="bodytext"> 
										<?php for($j=0; $j< count($VMdata); $j++){ ?>
											"<?php echo $VMdata[$j]['phylosophy']; ?>"
										<?php } ?> </p> 		
								</div>
							</div>
							</div>
						</div>
					</div> 
			</div>
		</div>
	</section>

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>