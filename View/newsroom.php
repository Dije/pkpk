	<!-- Header -->
	<?php $title = "Newsroom | PKPK";
	$page = "berita"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../Controller/newsController.php'); 
	$companyprofile = new companyprofileController();
	$news = new newsController();
	$CPdata = $companyprofile->getData();
	$newsData = $news->getDataTahun();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<!-- <div id="preloder">
			<div class="loader"></div>
		</div> -->

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar -->

		<!-- Subheader -->
		<?php include 'include/subheader.php' ?>
		<!-- Subheader -->

		<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"> 
						<div class="container">
							<div class="d-flex align-items-center">  
								<p class="hidden">Year:</p> 
								<select id="myInputNews" class="col-sm-4 form-control form-control-sm">
									<option value="" selected>All</option>
									<?php  foreach($newsData as $dtNews) { ?>
										<option value="<?php echo $dtNews['year']; ?>"><?php echo $dtNews['year']; ?></option> 
									<?php  } ?>
								</select>
								<br>
							</div>
							<?php     
							$perPage = 10;  
							$totalRecords = count($newsData);
							$totalPages = ceil($totalRecords/$perPage);
							?>   
							<table id="myTable1" class="table"> 
								<tbody id="contentNews"></tbody>  
							</table> 
							<nav aria-label="Page navigation example">
								<ul class="pagination justify-content-center"> 
									<div id="paginationNews"></div>    
								</ul>
							</nav>
							<input type="hidden" id="totalPagesNews" value="<?php echo $totalPages; ?>">    
						</div> 
					</div>  
				</div>
			</div>
		</div>
	</section>

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
