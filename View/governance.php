	<!-- Header -->
	<?php $title = "GOVERNANCE | PKPK";
	$page = "tata-kelola"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../Controller/homeController.php');
	$companyprofile = new companyprofileController();
	$home = new homeController();
	$CPdata = $companyprofile->getData();
	$indx = $home->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar --> 

		<!-- Hero Section Begin --> 
		<section class="cta-section spad set-bg opacity" data-setbg="admin/assets/img/esg/governance-header.png">
			<div class="text">
				<div class="cta-text">
					<h2>Governance</h2> 
					<br>
					<div class="text-sub"> <p style="white-space: pre-line;">We want to create a transparent, fair, 
and responsible corporate governance.</p> 
					</div>  
				</div>   
			</div>   
		</section>
		<!-- Hero Section End --> 

		<!-- NEWS Section Begin -->
		<section class="contact-section spad2">
			<div class="container">
				<div class="contact-text">
					<h3><b>What we do to make it happen</b></h3>  
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							We determined to create transparent, fair and responsible corporate governance  which in harmony with GCG (Good Corporate Governance) aspects. Those aspects include transparancy, accountability, responsibility, independence, and fairness. </p> 
						</div>
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							Therefore, while implementing ethical governance as above, PKPK is committed to continue build an innovative and clean business.</p> 
						</div>
					</div>   
					<div class="container">
						<div class="row">
							<!-- Hero Section Begin -->
								<section class="hero-section carouseelll">
									<div class="hs-slider owl-carousel normall"> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/governance/governance-1.png">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/governance/governance-2.JPG">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="admin/assets/img/esg/governance/governance-3.png">
										</div> 
									</div>
								</section>
							<!-- Hero Section End -->
						</div>
					</div>
				</div> 
			</div>
		</section>
		<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
