<!-- Header -->
<?php $title = "Good Corporate Governance | PKPK";
$page = "tata-kelola-perusahaan"; 
include 'include/header.php'; 
include_once (PROJECT_ROOT_PATH . '/../Controller/gcgController.php');
$gcg = new gcgController();
$gcgDt = $gcg->getData();
?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Subheader -->
	<?php include 'include/subheader.php' ?>
	<!-- Subheader -->

	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center">  
				<button class="button active" onClick="overviewFunction(this)"><a>Overview</a></button> 
				<button class="button" onClick="remunerationFunction(this)"><a>Remuneration and Nominating Committee</a></button> 
				<button class="button" onClick="nominatingFunction(this)"><a>Internal Audit Unit</a></button>  
				<button class="button" onClick="IControlFunction(this)"><a>Internal Control System</a></button>   
				<button class="button" onClick="RismanFunction(this)"><a>Risk Management System</a></button>  
				<button class="button" onClick="CoCFunction(this)"><a>Code Of Conduct</a></button>  
				<button class="button" onClick="WhistleFunction(this)"><a>Whistleblowing System</a></button>  
				<button class="button" onClick="InfFunction(this)"><a>Information and Data Access</a></button>   
			</div>
		</div>
	</section>

	<!-- Testimoial Section Begin -->
	<?php for($j=0; $j< count($gcgDt); $j++){ ?>
	<section class="testimonial-section">
		<div class="container"> 
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Overview"> 
						<p><?php echo $gcgDt[$j]['OverviewEng']; ?></p>
					</div>   
					<div class="section-title"id="Remuneration" style="display: none;">
						<p><?php echo $gcgDt[$j]['RaNEng']; ?></p>
					</div> 
					<div class="section-title"id="Nominating" style="display: none;">
						<p><?php echo $gcgDt[$j]['IAEng']; ?></p>
					</div> 
					<div class="section-title"id="Icontrol" style="display: none;">
						<p><?php echo $gcgDt[$j]['ICEng']; ?></p>
					</div>  
					<div class="section-title"id="RisMan" style="display: none;">
						<p><?php echo $gcgDt[$j]['RMEng']; ?></p>
					</div> 
					<div class="section-title"id="Coc" style="display: none;">
						<p><?php echo $gcgDt[$j]['COCEng']; ?></p>
					</div> 
					<div class="section-title"id="Whistleblowing" style="display: none;">
						<p><?php echo $gcgDt[$j]['WhistleEng']; ?></p>
					</div> 
					<div class="section-title"id="Information" style="display: none;">
						<p><?php echo $gcgDt[$j]['IaDEng']; ?></p>
					</div> 
				</div>  
			</div>
		</div>
	</section>
<?php } ?>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>
</html>