<!-- Header -->
<?php $title = "Corporate Social Responsibility | PKPK";
$page = "tanggung-jawab-sosial-perusahaan"; 
include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Subheader -->
	<?php include 'include/subheader.php' ?>
	<!-- Subheader -->
	
	<section class="testimonial-section">
		<div class="container">
			<div class="row justify-content-center">  
				<button class="button active" onClick="miningFunction(this)"><a>Overview</a></button>
				<button class="button" onClick="equipmentFunction(this)"><a>Environmental Preservation </a></button>
				<button class="button" onClick="landPreparationFunction(this)"><a>Employment, Occupational Health and Safety</a></button>
				<button class="button" onClick="constructionFunction(this)"><a>Social Affairs and Community Development</a></button>
				<button class="button" onClick="cProctFunction(this)"><a>Customer Protection</a></button>
			</div>
		</div>
	</section>
	
	<!-- Testimoial Section Begin -->
	<section class="testimonial-section">
		<div class="container">
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Mining">
						<p>As an integral part of society, the Corporate is committed to fostering mutual relationship with communities, through the implementation of Corporate Social Responsibility (CSR) program. The Company always carries out its corporate social responsibilities based on the applicable rules and regulations, including implementation in the social, environmental, and employment fields. CSR is part of the implementation of Good Corporate Governance. </p>
					</div>  
				</div>
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Equipment" style="display: none;">
						<p>The Company has prioritized environment management and other important factors to realize Company’s vision. The Company upholds its commitment to environmental preservation by consistently paying attention to the impact of its entire operations on the environment. The Company’s environmental CSR policy refers to the ISO 14001:2015.  

						In 2020, the Company implemented an environmental preservation program that included managing the project waste properly and in accordance with applicable regulations by separating waste according to the environmental management system.  </p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Land" style="display: none;">
						<p>The environment of construction work is full of dangerous risks for those engaged in construction business. Therefore, the Company strives to commit on the practice of occupational safety and health pursuant to the standard applied. In terms of employment, Law No. 13/2003 on Employment is the basis of all the Company’s employment policies, such as minimum wages, retirement savings program, and others. The Company also pays great attention to Occupational Safety and Health by implementing Occupational Safety and Health Management System (SMK3). 

						The Company also upholds its employment commitment and concerns by consistently enhancing OSH structure and implementation in the field. In addition, the Company persistently prioritizes their occupational safety and health by implementing various programs intended to raise their awareness regarding the importance of occupational safety and health</p>
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Construction" style="display: none;">  
						<p>Throughout 2020, the Company carried out community development around the work location, including through social and religious activities, and providing assistance to victims of natural disasters. The Company also provides Job Training Opportunity to vocational school students and internship. The Company also provide educational social assistance to orphans through the Rahmatullah Islamic Boarding School in Lempake, Samarinda. </p> 
					</div>
				</div>  
			</div>
			<div class="row">
				<div class="about-text">
					<div class="section-title"id="Cus" style="display: none;">  
						<p>To provide best quality and service to the customer, the Company is fully committed to upholding its responsibilities to all customers. To this end, the Company has provided communication channels for customers in the form of corporate e-mail address, phone, and others. The Company is committed to continuously improving customer services and prioritizing customer satisfaction.   </p> 
					</div>
				</div>  
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>