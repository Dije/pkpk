<?php
class newsController{
	protected $conn;
 
	public function __construct(){
		$this->conn = mysqli_connect("192.168.100.88", "deli", "Deli123", "website", "3306"); //(host, username, password, database, port)
	}

	// public function __construct(){
	// 	$this->conn = mysqli_connect("localhost", "pkpktbk1_pkpk", "Pkpk_1234!", "pkpktbk1_website", "3306"); //(host, username, password, database, port)
	// }
	
	public function getData(){
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE delete_date IS NULL ORDER BY created_date DESC");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){ 
			$data = 0;
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	} 

	public function getDataLatest(){
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE main_news = 0 AND delete_date IS NULL ORDER BY created_date DESC LIMIT 3");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){ 
			$data = 0;
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	} 

	public function getDataMain(){
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE main_news = 1 AND delete_date IS NULL ORDER BY created_date DESC LIMIT 1");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){ 
			$data = 0;
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	} 

	public function getDataByUid($uID){
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE id='$uID'");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	}

	public function getDataTahun(){
		$query = mysqli_query($this->conn,"SELECT DISTINCT year(created_date) AS year FROM news WHERE delete_date IS NULL ORDER BY created_date DESC");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	}

	public function getDatabyYear($year){
		$yearBefore = $year -1;
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE delete_date IS NULL AND year(created_date) > '$yearBefore' AND year(created_date) <= '$year' ORDER BY created_date DESC");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	} 


	public function getDataLimit($startFrom, $perPage){
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE delete_date IS NULL ORDER BY created_date DESC LIMIT $startFrom, $perPage");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	} 

	public function getDataLimitbyYear($startFrom, $perPage, $year){
		$yearBefore = $year -1;
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE delete_date IS NULL AND year(created_date) > '$yearBefore' AND year(created_date) <= '$year' ORDER BY created_date DESC LIMIT $startFrom, $perPage");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	} 

	public function getDataExcUid($uID){
		$query = mysqli_query($this->conn,"SELECT * FROM news WHERE id!='$uID' LIMIT 3");
		$jumdata= mysqli_num_rows($query);
		if($jumdata==0){
			$data="-";
		} else{
			while($row = mysqli_fetch_array($query)){
				$data[]=$row;
			}
		}
		return $data;
	}
	
	public function addNews($main, $titleEnglish, $NewsBodyEnglish, $titleIndonesia, $NewsBodyIndonesia, $nama2, $desc, $createddate){  
		$titleEnglish = mysqli_real_escape_string($this->conn,$titleEnglish); 
		$NewsBodyEnglish = mysqli_real_escape_string($this->conn,$NewsBodyEnglish); 
		$titleIndonesia = mysqli_real_escape_string($this->conn,$titleIndonesia); 
		$NewsBodyIndonesia = mysqli_real_escape_string($this->conn,$NewsBodyIndonesia); 
		$nama2 = mysqli_real_escape_string($this->conn,$nama2); 
		$desc = mysqli_real_escape_string($this->conn,$desc);  
		$query="INSERT INTO news(main_news, news_title_english, news_title_indonesia, news_body_english, news_body_indonesia, image, remark, created_date) VALUES ('$main', '$titleEnglish', '$titleIndonesia', '$NewsBodyEnglish', '$NewsBodyIndonesia', '$nama2', '$desc', '$createddate')";
		$result = mysqli_query($this->conn,$query) or die(mysqli_connect_errno()."Data cannot inserted");
		return $result; 
	} 

	public function updateDataByUID($main, $titleEnglish, $NewsBodyEnglish, $titleIndonesia, $NewsBodyIndonesia, $nama2, $desc, $updatedate, $uID){  
		$titleEnglish = mysqli_real_escape_string($this->conn,$titleEnglish); 
		$NewsBodyEnglish = mysqli_real_escape_string($this->conn,$NewsBodyEnglish); 
		$titleIndonesia = mysqli_real_escape_string($this->conn,$titleIndonesia); 
		$NewsBodyIndonesia = mysqli_real_escape_string($this->conn,$NewsBodyIndonesia); 
		$nama2 = mysqli_real_escape_string($this->conn,$nama2); 
		$desc = mysqli_real_escape_string($this->conn,$desc);  
		$query = "UPDATE news SET main_news = '$main', news_title_english = '$titleEnglish', news_title_indonesia = '$titleIndonesia', news_body_english = '$NewsBodyEnglish', news_body_indonesia = '$NewsBodyIndonesia', image = '$nama2', remark = '$desc', update_date = '$updatedate' WHERE id = '$uID'";
		$result = mysqli_query($this->conn,$query) or die(mysqli_error()."Data cannot update");
		return $result; 
	} 

	public function updateDataWithoutFileByUID($main, $titleEnglish, $NewsBodyEnglish, $titleIndonesia, $NewsBodyIndonesia, $desc, $updatedate, $uID){  
		$titleEnglish = mysqli_real_escape_string($this->conn,$titleEnglish); 
		$NewsBodyEnglish = mysqli_real_escape_string($this->conn,$NewsBodyEnglish); 
		$titleIndonesia = mysqli_real_escape_string($this->conn,$titleIndonesia); 
		$NewsBodyIndonesia = mysqli_real_escape_string($this->conn,$NewsBodyIndonesia);  
		$desc = mysqli_real_escape_string($this->conn,$desc);  
		$query = "UPDATE news SET main_news = '$main', news_title_english = '$titleEnglish', news_title_indonesia = '$titleIndonesia', news_body_english = '$NewsBodyEnglish', news_body_indonesia = '$NewsBodyIndonesia', remark = '$desc', update_date = '$updatedate' WHERE id = '$uID'";
		$result = mysqli_query($this->conn,$query) or die(mysqli_error()."Data cannot update");
		return $result; 
	} 

	// public function deleteReport($deletedate, $IDReport){
	// 	$query = "SELECT * FROM news WHERE ID_M='$IDReport'";
    //         //checking if the data is available in db
	// 	$result = mysqli_query($this->conn,$query);
	// 	$count_row = $result->num_rows;
	// 	if ($count_row == 1){
	// 		$query = "UPDATE news SET delete_date = '$deletedate' WHERE ID_M='$IDReport'";
	// 		$result = mysqli_query($this->conn,$query) or die(mysqli_connect_errno()."Data cannot inserted");
	// 		return $result; 
	// 	}
	// 	else { 
	// 		return false;
	// 	}
	// }
}
?>

