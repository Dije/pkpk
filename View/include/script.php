<!-- Js Plugins -->
<script src="js/jquery-3.3.1.min.js"></script> 
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.easing.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/masonry.pkgd.min.js"></script>
<script src="js/jquery.slicknav.js"></script> 
<script src="js/main.js"></script>
<script src="js/footer.js"></script> 
<script src="https://www.google.com/recaptcha/api.js"></script>
<script src="assets/recaptcha/validator.js"></script> 
<script src="assets/recaptcha/contact.js"></script>  
<script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script> 
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/exporting.js"></script>
<script src="https://code.highcharts.com/modules/export-data.js"></script>
<script src="https://code.highcharts.com/modules/accessibility.js"></script>

<script src="admin/plugins/simple-bootstrap-paginator/simple-bootstrap-paginator.js"></script>

<script>
(function() {
    var cx = 'caf30d28032f80ffc';
    var gcse = document.createElement('script');
    gcse.type = 'text/javascript';
    gcse.async = true;
    gcse.src = (document.location.protocol == 'https:' ? 'https:' : 'http:') + '//www.google.com/cse/cse.js?cx=' + cx;
    var s = document.getElementsByTagName('script')[0];
    s.parentNode.insertBefore(gcse, s);
})();
	window.onload = function(){
	document.getElementById('gsc-i-id1').placeholder = 'Search...';
	document.getElementById('gsc-i-id2').placeholder = 'Search...';
}
</script>

<script>
	$(function() {
	  AOS.init();
	});
</script>

<script >
	$('#click_advance0').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script >
	$('#click_advance1').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	$('#click_advance2').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	$('#click_advance3').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	$('#click_advance4').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	$('#click_advance5').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	$('#click_advance6').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	$('#click_advance7').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance8').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance8').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}   
	});	
</script>
<script>
	$('#click_advance8').click(function() {
		$("i", this).toggleClass("fa fa-angle-double-down fa fa-angle-double-up");
		if($("i", '#click_advance0').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance0').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance1').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance1').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}   
		if($("i", '#click_advance2').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance2').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance3').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance3').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance4').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance4').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance5').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance5').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
		if($("i", '#click_advance6').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance6').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		}  
		if($("i", '#click_advance7').hasClass('fa fa-angle-double-up')){
			$("i", '#click_advance7').toggleClass("fa fa-angle-double-up fa fa-angle-double-down");
		} 
	});	
</script>
<script>
	function myFunction0() { 
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (x.style.display === "none") {
			x.style.display = "block";
			y.style.display = "none";
			z.style.display = "none";
			a.style.display = "none";
			b.style.display = "none";
			c.style.display = "none";
			d.style.display = "none";
			e.style.display = "none";
			f.style.display = "none";
		} else {
			x.style.display = "none";
		}
	} 
	function myFunction1() {  
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (y.style.display === "none") {
			x.style.display = "none";
			y.style.display = "block";
			z.style.display = "none";
			a.style.display = "none";
			b.style.display = "none";
			c.style.display = "none";
			d.style.display = "none";
			e.style.display = "none";
			f.style.display = "none";
		} else {
			y.style.display = "none";
		}
	} 
	function myFunction2() {
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (z.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "block";
			a.style.display = "none";
			b.style.display = "none";
			c.style.display = "none";
			d.style.display = "none";
			e.style.display = "none";
			f.style.display = "none";
		} else {
			z.style.display = "none";
		}
	} 
	function myFunction3() {  
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (a.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "none"; 
			a.style.display = "block";
			b.style.display = "none";
			c.style.display = "none";
			d.style.display = "none";
			e.style.display = "none";
			f.style.display = "none";
		} else {
			a.style.display = "none";
		}
	} 
	function myFunction4() {
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (b.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "none"; 
			a.style.display = "none";
			b.style.display = "block";
			c.style.display = "none";
			d.style.display = "none";
			e.style.display = "none";
			f.style.display = "none";
		} else {
			b.style.display = "none";
		}
	} 

	function myFunction5() {
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (c.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "none"; 
			a.style.display = "none";
			b.style.display = "none";
			c.style.display = "block";
			d.style.display = "none";
			e.style.display = "none";
			f.style.display = "none";
		} else {
			c.style.display = "none";
		}
	} 

	function myFunction6() {
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (d.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "none"; 
			a.style.display = "none";
			b.style.display = "none";
			c.style.display = "none";
			d.style.display = "block";
			e.style.display = "none";
			f.style.display = "none";
		} else {
			d.style.display = "none";
		}
	} 

	function myFunction7() {
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (e.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "none"; 
			a.style.display = "none";
			b.style.display = "none";
			c.style.display = "none";
			d.style.display = "none";
			e.style.display = "block";
			f.style.display = "none";
		} else {
			e.style.display = "none";
		}
	} 

	function myFunction8() {
		var x = document.getElementById("myDIV0");
		var y = document.getElementById("myDIV1");
		var z = document.getElementById("myDIV2");
		var a = document.getElementById("myDIV3");
		var b = document.getElementById("myDIV4");
		var c = document.getElementById("myDIV5");
		var d = document.getElementById("myDIV6");
		var e = document.getElementById("myDIV7");
		var f = document.getElementById("myDIV8"); 
		if (f.style.display === "none") {
			x.style.display = "none";
			y.style.display = "none";
			z.style.display = "none"; 
			a.style.display = "none";
			b.style.display = "none";
			c.style.display = "none";
			d.style.display = "none";
			e.style.display = "none";
			f.style.display = "block";
		} else {
			f.style.display = "none";
		}
	} 
</script>

<script>
	function overviewFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "block";   
			c.style.display = "none"; 
			d.style.display = "none";  
			g.style.display = "none";  
			j.style.display = "none";   
			k.style.display = "none"; 
			l.style.display = "none"; 
			m.style.display = "none"; 
			
		} else {
			// a.style.display = "none";
		}
	}
	function remunerationFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (c.style.display === "none") {
			a.style.display = "none"; 
			c.style.display = "block"; 
			d.style.display = "none"; 
			g.style.display = "none";   
			j.style.display = "none";   
			k.style.display = "none"; 
			l.style.display = "none"; 
			m.style.display = "none"; 
		} else {
			// c.style.display = "none";
		}
	}
	function nominatingFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (d.style.display === "none") {
			a.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "block"; 
			g.style.display = "none";   
			j.style.display = "none";
			k.style.display = "none"; 
			l.style.display = "none"; 
			m.style.display = "none"; 
		} else {
			// d.style.display = "none";
		}
	}
	function IControlFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (g.style.display === "none") {
			a.style.display = "none";  
			c.style.display = "none"; 
			d.style.display = "none";  
			g.style.display = "block";   
			j.style.display = "none";
			k.style.display = "none";   
			l.style.display = "none"; 
			m.style.display = "none"; 
		} else {
			// g.style.display = "none";
		}
	}
	function RismanFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (j.style.display === "none") {
			a.style.display = "none";  
			c.style.display = "none"; 
			d.style.display = "none";  
			g.style.display = "none"; 
			j.style.display = "block"; 
			k.style.display = "none";   
			l.style.display = "none"; 
			m.style.display = "none";         
		} else {
			// j.style.display = "none";
		}
	}
	function CoCFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (k.style.display === "none") {
			a.style.display = "none";  
			c.style.display = "none"; 
			d.style.display = "none";  
			g.style.display = "none"; 
			j.style.display = "none"; 
			k.style.display = "block";
			l.style.display = "none"; 
			m.style.display = "none"; 
		} else {
			// k.style.display = "none";
		}
	}
	function WhistleFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (l.style.display === "none") {
			a.style.display = "none";  
			c.style.display = "none"; 
			d.style.display = "none";  
			g.style.display = "none"; 
			j.style.display = "none"; 
			k.style.display = "none"; 
			l.style.display = "block"; 
			m.style.display = "none"; 
		} else {
			// l.style.display = "none";
		}
	}
	function InfFunction(elem) {
		var a = document.getElementById("Overview"); 
		var c = document.getElementById("Remuneration");
		var d = document.getElementById("Nominating"); 
		var g = document.getElementById("Icontrol"); 
		var j = document.getElementById("RisMan");
		var k = document.getElementById("Coc");
		var l = document.getElementById("Whistleblowing");
		var m = document.getElementById("Information"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (m.style.display === "none") {
			a.style.display = "none";  
			c.style.display = "none"; 
			d.style.display = "none";  
			g.style.display = "none"; 
			j.style.display = "none"; 
			k.style.display = "none";   
			l.style.display = "none"; 
			m.style.display = "block"; 
		} else {
			// m.style.display = "none";
		}
	}
</script>

<script>
	function vissionFunction(elem) {
		var a = document.getElementById("Vision");
		var b = document.getElementById("Mission");
		var c = document.getElementById("Motto");
		var d = document.getElementById("Phylosophy");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "flex";  
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "none";     
		} else {
			// a.style.display = "none";
		}
	}
	function missionFunction(elem) {
		var a = document.getElementById("Vision");
		var b = document.getElementById("Mission");
		var c = document.getElementById("Motto");
		var d = document.getElementById("Phylosophy");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none";  
			b.style.display = "flex"; 
			c.style.display = "none"; 
			d.style.display = "none";  
		} else {
			// b.style.display = "none";
		}
	}
	function mottoFunction(elem) {
		var a = document.getElementById("Vision");
		var b = document.getElementById("Mission");
		var c = document.getElementById("Motto");
		var d = document.getElementById("Phylosophy");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (c.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "flex"; 
			d.style.display = "none";  
		} else {
			// c.style.display = "none";
		}
	}
	function phylosophyFunction(elem) {
		var a = document.getElementById("Vision");
		var b = document.getElementById("Mission");
		var c = document.getElementById("Motto");
		var d = document.getElementById("Phylosophy");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (d.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "flex";  
		} else {
			// d.style.display = "none";
		}
	}
</script>

<script>
	function miningFunction(elem) {
		var a = document.getElementById("Mining");
		var b = document.getElementById("Equipment");
		var c = document.getElementById("Land");
		var d = document.getElementById("Construction");
		var e = document.getElementById("Cus");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "flex";  
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "none";     
			e.style.display = "none";   
		} else {
			// a.style.display = "none";
		}
	}
	function equipmentFunction(elem) {
		var a = document.getElementById("Mining");
		var b = document.getElementById("Equipment");
		var c = document.getElementById("Land");
		var d = document.getElementById("Construction");
		var e = document.getElementById("Cus");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none";  
			b.style.display = "flex"; 
			c.style.display = "none"; 
			d.style.display = "none";    
			e.style.display = "none"; 
		} else {
			// b.style.display = "none";
		}
	}
	function landPreparationFunction(elem) {
		var a = document.getElementById("Mining");
		var b = document.getElementById("Equipment");
		var c = document.getElementById("Land");
		var d = document.getElementById("Construction");
		var e = document.getElementById("Cus");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (c.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "flex"; 
			d.style.display = "none";  
			e.style.display = "none";   
		} else {
			// c.style.display = "none";
		}
	}
	function constructionFunction(elem) {
		var a = document.getElementById("Mining");
		var b = document.getElementById("Equipment");
		var c = document.getElementById("Land");
		var d = document.getElementById("Construction");
		var e = document.getElementById("Cus");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (d.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "flex";  
			e.style.display = "none"; 
		} else {
			// d.style.display = "none";
		}
	}
	function cProctFunction(elem) {
		var a = document.getElementById("Mining");
		var b = document.getElementById("Equipment");
		var c = document.getElementById("Land");
		var d = document.getElementById("Construction");
		var e = document.getElementById("Cus");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (e.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "none"; 
			d.style.display = "none";  
			e.style.display = "flex";  
		} else {
			// e.style.display = "none";
		}
	}
</script>

<script>
	function FinishedFunction(elem) {
		var a = document.getElementById("Finish");
		var b = document.getElementById("Current"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "flex"; 
			b.style.display = "none";  
		} else {
			// a.style.display = "none";
		}
	}
	function CurrentFunction(elem) {
		var a = document.getElementById("Finish");
		var b = document.getElementById("Current"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "flex";   
		} else {
			// b.style.display = "none";
		}
	}
</script>

<script>
	function overviewBusinessFunction(elem) {
		var a = document.getElementById("OverviewBusiness");
		var b = document.getElementById("TradingRiskmanagement"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "block"; 
			b.style.display = "none";  
		} else {
			// a.style.display = "none";
		}
	}
	function TradingRiskManagementFunction(elem) {
		var a = document.getElementById("OverviewBusiness");
		var b = document.getElementById("TradingRiskmanagement"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "block";  
		} else {
			// b.style.display = "none";
		}
	}
</script>

<script>
	function overviewShippingFunction(elem) {
		var a = document.getElementById("OverviewShipping");
		var b = document.getElementById("Videos"); 
		var c = document.getElementById("Fleet");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (a.style.display === "none") {
			a.style.display = "block"; 
			b.style.display = "none";  
			c.style.display = "none"; 
		} else {
			// a.style.display = "none";
		}
	}
	function videosFunction(elem) {
		var a = document.getElementById("OverviewShipping");
		var b = document.getElementById("Videos"); 
		var c = document.getElementById("Fleet");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "block";  
			c.style.display = "none"; 
		} else {
			// b.style.display = "none";
		}
	}
	function fleetFunction(elem) {
		var a = document.getElementById("OverviewShipping");
		var b = document.getElementById("Videos");
		var c = document.getElementById("Fleet");
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		elem.classList.add('active'); 
		if (c.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "none"; 
			c.style.display = "block";  
		} else {
			// c.style.display = "none";
		}
	}
</script>

<script>
	$('#myModal').on('shown.bs.modal', function () {
		$('#video')[0].play();
	});
	$('#myModal2').on('shown.bs.modal', function () {
		$('#video2')[0].play();
	});
	$('#myModal3').on('shown.bs.modal', function () {
		$('#video3')[0].play();
	});
	$(window).on('hidden.bs.modal', function () {
		$('#video')[0].pause();
		$('#video2')[0].pause();
		$('#video3')[0].pause();
	})
</script>  

<script>
$(document).ready(function(){
	$('[data-toggle="popover"]').popover({ 
		content: '<a class="close1" data-dismiss="alert" href="#/">Salim Limanto</a>',
		html: true
	}); 
	$(document).on("click", ".popover .close1" , function(){
		$(this).parents(".popover").popover('hide');
		$('#myModal4').modal('show');
	});
});
</script>  

<script>
$('.normal').owlCarousel({
    loop:true, 
    nav:false,
    autoplay:false,
    autoplayTimeout:9000,
    dots: false,
    responsive:{
        0:{
            items:1
        },
        640:{
            items:1
        },
      	960:{
            items:1
        },
        1200:{
            items:1
        }
    } 
})
</script> 

<script>
$('.normall').owlCarousel({
    loop:true, 
    nav:true,
    autoplay:true,
    autoplayTimeout:9000,
    dots: false,
    responsive:{
        0:{
            items:1
        },
        640:{
            items:1
        },
      	960:{
            items:1
        },
        1200:{
            items:1
        }
    } 
})
</script> 

<script>
/*Scroll to top when arrow up clicked BEGIN*/
$(window).scroll(function() {
    var height = $(window).scrollTop();
    if (height > 100) {
        $('#back2Top').fadeIn();
    } else {
        $('#back2Top').fadeOut();
    }
});
$(document).ready(function() {
    $("#back2Top").click(function(event) {
        event.preventDefault();
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });

});
 /*Scroll to top when arrow up clicked END*/
 </script>  

<script>
$(function () { 
    if (window.location.hash === "#TradingRiskManagement") { 
        var a = document.getElementById("OverviewBusiness");
		var b = document.getElementById("TradingRiskmanagement"); 
		var btn = document.getElementsByTagName("button")
		for (i = 0; i < btn.length; i++) {
			btn[i].classList.remove('active');
		}
		btn[1].classList.add('active');
		if (b.style.display === "none") {
			a.style.display = "none"; 
			b.style.display = "block";  
		} else {
			// b.style.display = "none";
		}
    } 
}); 
</script> 	

<script>
Highcharts.chart('container', {
    chart: {
        type: 'line'
    },
    title: {
        text: 'AUDIT'
    }, 
    xAxis: {
        categories: <?php echo json_encode($Year, JSON_NUMERIC_CHECK); ?>
    },
    yAxis: {
        title: {
            text: 'Financial Year End 31 Dec (IDR)'
        }
    },
    plotOptions: {
        line: {
            dataLabels: {
                enabled: false
            },
            enableMouseTracking: true
        }
    },
    series: [{
        name: 'Revenue',
        data: <?php echo json_encode($Revenue, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'Profit before tax',
        data: <?php echo json_encode($ProfitBfTax, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'Profit attributable to equity holders of the Company',
        data: <?php echo json_encode($ProfitAttributable, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'Earnings per share (1) (cents)',
        data: <?php echo json_encode($EarningsPerShare, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'Non-current assets',
        data: <?php echo json_encode($NonCurrentAsset, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'Current assets',
        data: <?php echo json_encode($CurrentAssets, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'Non-current liabilities',
        data: <?php echo json_encode($NonCurrentLia, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'Current liabilities',
        data: <?php echo json_encode($CurrentLia, JSON_NUMERIC_CHECK); ?>
    }, {
        name: '(NAV) attributable to equity holders of the Company',
        data: <?php echo json_encode($AttEquity, JSON_NUMERIC_CHECK); ?>
    }, {
        name: 'NAV per share (1) (cents)',
        data: <?php echo json_encode($NavPerShare, JSON_NUMERIC_CHECK); ?>
    }]
});
</script>  
 
<script type="text/javascript">
    $(document).ready(function(){
        var totalPage = parseInt($('#totalPages').val());   
        console.log("==totalPage=="+totalPage);

        var pag = $('#pagination').simplePaginator({
            totalPages: totalPage,
            maxButtonsVisible: 5,
            currentPage: 1,
            nextLabel: 'Next',
            prevLabel: 'Prev',
            firstLabel: 'First',
            lastLabel: 'Last',
            clickCurrentPage: true,
            pageChange: function(page) {    
                $("#content").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
                $.ajax({
                    url:"load_data_financial_statement.php",
                    method:"POST",
                    dataType: "json",       
                    data:{page: page, year:""},
                    success:function(responseData){ 
                        $('#content').html(responseData.html);
                    } 
                });
            }       
        });
    });
</script>


<script type="text/javascript">
	$(document).ready(function(){ 
	    $("#myInput1").on("change",function(){
	    	var year = $(this).val(); 
	        $.ajax({
		        url :"total_data_financial_statement.php",
		        type:"POST",
		        cache:false,
		        data: 'year=' + year,
		        success:function(response){  
					var obj=$.parseJSON(response);   
		        	$('#totalPages').val(obj.totalData);   

			    	var totalPage = parseInt($('#totalPages').val());   
			    	console.log("==totalPage=="+totalPage);
			      
			        $('#pagination').simplePaginator({
			            totalPages: totalPage,
			            maxButtonsVisible: 5,
			            currentPage: 1,
			            nextLabel: 'Next',
			            prevLabel: 'Prev',
			            firstLabel: 'First',
			            lastLabel: 'Last',
			            clickCurrentPage: true,
			            pageChange: function(page) {  
			                $("#content").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
			                $.ajax({
			                    url:"load_data_financial_statement.php",
			                    method:"POST",
			                    dataType: "json",       
			                    data:{page: page, year: year},
			                    success:function(responseData){ 
			                        $('#content').html(responseData.html);
			                    },
						        error: function(jqXHR, textStatus, errorThrown) {
						           console.log(textStatus, errorThrown);
						        }
			                });
			            }       
			        });
		        }
			});
	    }); 
	});
</script>  

<script type="text/javascript">
    $(document).ready(function(){
        var totalPageAnnual = parseInt($('#totalPagesAnnual').val());   
        console.log("==totalPageAnnual=="+totalPageAnnual);

        $('#paginationAnnual').simplePaginator({
            totalPages: totalPageAnnual,
            maxButtonsVisible: 5,
            currentPage: 1,
            nextLabel: 'Next',
            prevLabel: 'Prev',
            firstLabel: 'First',
            lastLabel: 'Last',
            clickCurrentPage: true,
            pageChange: function(page) {    
                $("#contentAnnual").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
                $.ajax({
                    url:"load_data_annual_report.php",
                    method:"POST",
                    dataType: "json",     
                    data:{page: page, year:""},
                    success:function(responseData){ 
                        $('#contentAnnual').html(responseData.html);
                    } 
                });
            }       
        });
    });
</script>


<script type="text/javascript">
	$(document).ready(function(){ 
	    $("#myInput").on("change",function(){
	    	var year = $(this).val();  
	        $.ajax({
		        url :"total_data_annual_report.php",
		        type:"POST",
		        cache:false,
		        data: 'year=' + year,
		        success:function(response){  
					var obj=$.parseJSON(response);   
		        	$('#totalPagesAnnual').val(obj.totalData);   

			    	var totalPageAnnual = parseInt($('#totalPagesAnnual').val());   
			    	console.log("==totalPageAnnual=="+totalPageAnnual);
			      
			        $('#pagination').simplePaginator({
			            totalPages: totalPageAnnual,
			            maxButtonsVisible: 5,
			            currentPage: 1,
			            nextLabel: 'Next',
			            prevLabel: 'Prev',
			            firstLabel: 'First',
			            lastLabel: 'Last',
			            clickCurrentPage: true,
			            pageChange: function(page) {  
			                $("#contentAnnual").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
			                $.ajax({
			                    url:"load_data_annual_report.php",
			                    method:"POST",
			                    dataType: "json",       
			                    data:{page: page, year: year},
			                    success:function(responseData){ 
			                        $('#contentAnnual').html(responseData.html);
			                    },
						        error: function(jqXHR, textStatus, errorThrown) {
						           console.log(textStatus, errorThrown);
						        }
			                });
			            }       
			        });
		        }
			});
	    }); 
	});
</script>  


<script type="text/javascript">
    $(document).ready(function(){
        var totalPagesNews = parseInt($('#totalPagesNews').val());   
        console.log("==totalPagesNews=="+totalPagesNews);

        $('#paginationNews').simplePaginator({
            totalPages: totalPagesNews,
            maxButtonsVisible: 5,
            currentPage: 1,
            nextLabel: 'Next',
            prevLabel: 'Prev',
            firstLabel: 'First',
            lastLabel: 'Last',
            clickCurrentPage: true,
            pageChange: function(page) {    
                $("#contentNews").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
                $.ajax({
                    url:"load_data_news.php",
                    method:"POST",
                    dataType: "json",     
                    data:{page: page, year:""},
                    success:function(responseData){ 
                        $('#contentNews').html(responseData.html);
                    } 
                });
            }       
        });
    });
</script>

<script type="text/javascript">
	$(document).ready(function(){ 
	    $("#myInputNews").on("change",function(){
	    	var year = $(this).val();   
	        $.ajax({
		        url :"total_data_news.php",
		        type:"POST",
		        cache:false,
		        data: 'year=' + year,
		        success:function(response){  
					var obj=$.parseJSON(response);   
		        	$('#totalPagesNews').val(obj.totalData);   

			    	var totalPagesNews = parseInt($('#totalPagesNews').val());   
			    	console.log("==totalPagesNews=="+totalPagesNews);
			      
			        $('#paginationNews').simplePaginator({
			            totalPages: totalPagesNews,
			            maxButtonsVisible: 5,
			            currentPage: 1,
			            nextLabel: 'Next',
			            prevLabel: 'Prev',
			            firstLabel: 'First',
			            lastLabel: 'Last',
			            clickCurrentPage: true,
			            pageChange: function(page) {  
			                $("#contentNews").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
			                $.ajax({
			                    url:"load_data_news.php",
			                    method:"POST",
			                    dataType: "json",       
			                    data:{page: page, year: year},
			                    success:function(responseData){ 
			                        $('#contentNews').html(responseData.html);
			                    },
						        error: function(jqXHR, textStatus, errorThrown) {
						           console.log(textStatus, errorThrown);
						        }
			                });
			            }       
			        });
		        }
			});
	    }); 
	});
</script>  

<script type="text/javascript">
    $(document).ready(function(){
        var totalPageAnnounce = parseInt($('#totalPagesAnnounce').val());   
        console.log("==totalPageAnnounce=="+totalPageAnnounce);

        var pag = $('#pagination2').simplePaginator({
            totalPages: totalPageAnnounce,
            maxButtonsVisible: 5,
            currentPage: 1,
            nextLabel: 'Next',
            prevLabel: 'Prev',
            firstLabel: 'First',
            lastLabel: 'Last',
            clickCurrentPage: true,
            pageChange: function(page) {    
                $("#contentAnnounce").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
                $.ajax({
                    url:"load_data_rups_announcement.php",
                    method:"POST",
                    dataType: "json",       
                    data:{page: page, year:""},
                    success:function(responseData){ 
                        $('#contentAnnounce').html(responseData.html);
                    } 
                });
            }       
        });
    });
</script>


<script type="text/javascript">
	$(document).ready(function(){ 
	    $("#myInput2").on("change",function(){
	    	var year = $(this).val(); 
	        $.ajax({
		        url :"total_data_rups_announcement.php",
		        type:"POST",
		        cache:false,
		        data: 'year=' + year,
		        success:function(response){  
					var obj=$.parseJSON(response);   
		        	$('#totalPagesAnnounce').val(obj.totalData);   

			    	var totalPageAnnounce = parseInt($('#totalPagesAnnounce').val());   
			    	console.log("==totalPageAnnounce=="+totalPageAnnounce);
			      
			        $('#pagination2').simplePaginator({
			            totalPages: totalPageAnnounce,
			            maxButtonsVisible: 5,
			            currentPage: 1,
			            nextLabel: 'Next',
			            prevLabel: 'Prev',
			            firstLabel: 'First',
			            lastLabel: 'Last',
			            clickCurrentPage: true,
			            pageChange: function(page) {  
			                $("#contentAnnounce").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
			                $.ajax({
			                    url:"load_data_rups_announcement.php",
			                    method:"POST",
			                    dataType: "json",       
			                    data:{page: page, year: year},
			                    success:function(responseData){ 
			                        $('#contentAnnounce').html(responseData.html);
			                    },
						        error: function(jqXHR, textStatus, errorThrown) {
						           console.log(textStatus, errorThrown);
						        }
			                });
			            }       
			        });
		        }
			});
	    }); 
	});
</script>  

<script type="text/javascript">
    $(document).ready(function(){
        var totalPageInvitation = parseInt($('#totalPagesInvitation').val());   
        console.log("==totalPageInvitation=="+totalPageInvitation);

        var pag = $('#paginationInvitation').simplePaginator({
            totalPages: totalPageInvitation,
            maxButtonsVisible: 5,
            currentPage: 1,
            nextLabel: 'Next',
            prevLabel: 'Prev',
            firstLabel: 'First',
            lastLabel: 'Last',
            clickCurrentPage: true,
            pageChange: function(page) {    
                $("#contentInvitation").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
                $.ajax({
                    url:"load_data_rups_invitation.php",
                    method:"POST",
                    dataType: "json",       
                    data:{page: page, year:""},
                    success:function(responseData){ 
                        $('#contentInvitation').html(responseData.html);
                    } 
                });
            }       
        });
    });
</script>


<script type="text/javascript">
	$(document).ready(function(){ 
	    $("#myInput3").on("change",function(){
	    	var year = $(this).val(); 
	        $.ajax({
		        url :"total_data_rups_invitation.php",
		        type:"POST",
		        cache:false,
		        data: 'year=' + year,
		        success:function(response){  
					var obj=$.parseJSON(response);   
		        	$('#totalPagesInvitation').val(obj.totalData);   

			    	var totalPageInvitation = parseInt($('#totalPagesInvitation').val());   
			    	console.log("==totalPageInvitation=="+totalPageInvitation);
			      
			        $('#paginationInvitation').simplePaginator({
			            totalPages: totalPageInvitation,
			            maxButtonsVisible: 5,
			            currentPage: 1,
			            nextLabel: 'Next',
			            prevLabel: 'Prev',
			            firstLabel: 'First',
			            lastLabel: 'Last',
			            clickCurrentPage: true,
			            pageChange: function(page) {  
			                $("#contentInvitation").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
			                $.ajax({
			                    url:"load_data_rups_invitation.php",
			                    method:"POST",
			                    dataType: "json",       
			                    data:{page: page, year: year},
			                    success:function(responseData){ 
			                        $('#contentInvitation').html(responseData.html);
			                    },
						        error: function(jqXHR, textStatus, errorThrown) {
						           console.log(textStatus, errorThrown);
						        }
			                });
			            }       
			        });
		        }
			});
	    }); 
	});
</script>  

<script type="text/javascript">
    $(document).ready(function(){
        var totalPageMOM = parseInt($('#totalPagesMOM').val());   
        console.log("==totalPageMOM=="+totalPageMOM);

        var pag = $('#paginationMOM').simplePaginator({
            totalPages: totalPageMOM,
            maxButtonsVisible: 5,
            currentPage: 1,
            nextLabel: 'Next',
            prevLabel: 'Prev',
            firstLabel: 'First',
            lastLabel: 'Last',
            clickCurrentPage: true,
            pageChange: function(page) {    
                $("#contentMOM").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
                $.ajax({
                    url:"load_data_rups_mom.php",
                    method:"POST",
                    dataType: "json",       
                    data:{page: page, year:""},
                    success:function(responseData){ 
                        $('#contentMOM').html(responseData.html);
                    } 
                });
            }       
        });
    });
</script>


<script type="text/javascript">
	$(document).ready(function(){ 
	    $("#myInput4").on("change",function(){
	    	var year = $(this).val(); 
	        $.ajax({
		        url :"total_data_rups_mom.php",
		        type:"POST",
		        cache:false,
		        data: 'year=' + year,
		        success:function(response){  
					var obj=$.parseJSON(response);   
		        	$('#totalPagesMOM').val(obj.totalData);   

			    	var totalPageMOM = parseInt($('#totalPagesMOM').val());   
			    	console.log("==totalPageMOM=="+totalPageMOM);
			      
			        $('#paginationMOM').simplePaginator({
			            totalPages: totalPageMOM,
			            maxButtonsVisible: 5,
			            currentPage: 1,
			            nextLabel: 'Next',
			            prevLabel: 'Prev',
			            firstLabel: 'First',
			            lastLabel: 'Last',
			            clickCurrentPage: true,
			            pageChange: function(page) {  
			                $("#contentMOM").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
			                $.ajax({
			                    url:"load_data_rups_mom.php",
			                    method:"POST",
			                    dataType: "json",       
			                    data:{page: page, year: year},
			                    success:function(responseData){ 
			                        $('#contentMOM').html(responseData.html);
			                    },
						        error: function(jqXHR, textStatus, errorThrown) {
						           console.log(textStatus, errorThrown);
						        }
			                });
			            }       
			        });
		        }
			});
	    }); 
	});
</script>  

<script type="text/javascript">
    $(document).ready(function(){
        var totalPageDisclosure = parseInt($('#totalPagesDisclosure').val());   
        console.log("==totalPageDisclosure=="+totalPageDisclosure);

        var pag = $('#paginationDisclosure').simplePaginator({
            totalPages: totalPageDisclosure,
            maxButtonsVisible: 5,
            currentPage: 1,
            nextLabel: 'Next',
            prevLabel: 'Prev',
            firstLabel: 'First',
            lastLabel: 'Last',
            clickCurrentPage: true,
            pageChange: function(page) {    
                $("#contentDisclosure").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
                $.ajax({
                    url:"load_data_disclosure.php",
                    method:"POST",
                    dataType: "json",       
                    data:{page: page, year:""},
                    success:function(responseData){ 
                        $('#contentDisclosure').html(responseData.html);
                    } 
                });
            }       
        });
    });
</script>

<script type="text/javascript">
	$(document).ready(function(){ 
	    $("#myInput5").on("change",function(){
	    	var year = $(this).val(); 
	        $.ajax({
		        url :"total_data_disclosure.php",
		        type:"POST",
		        cache:false,
		        data: 'year=' + year,
		        success:function(response){  
					var obj=$.parseJSON(response);   
		        	$('#totalPagesDisclosure').val(obj.totalData);   

			    	var totalPageDisclosure = parseInt($('#totalPagesDisclosure').val());   
			    	console.log("==totalPageDisclosure=="+totalPageDisclosure);
			      
			        $('#paginationDisclosure').simplePaginator({
			            totalPages: totalPageDisclosure,
			            maxButtonsVisible: 5,
			            currentPage: 1,
			            nextLabel: 'Next',
			            prevLabel: 'Prev',
			            firstLabel: 'First',
			            lastLabel: 'Last',
			            clickCurrentPage: true,
			            pageChange: function(page) {  
			                $("#contentDisclosure").html('<tr><td colspan="6"><strong>loading...</strong></td></tr>');
			                $.ajax({
			                    url:"load_data_disclosure.php",
			                    method:"POST",
			                    dataType: "json",       
			                    data:{page: page, year: year},
			                    success:function(responseData){ 
			                        $('#contentDisclosure').html(responseData.html);
			                    },
						        error: function(jqXHR, textStatus, errorThrown) {
						           console.log(textStatus, errorThrown);
						        }
			                });
			            }       
			        });
		        }
			});
	    }); 
	});
</script>   