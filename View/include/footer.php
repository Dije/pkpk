<a id="back2Top" href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

<!-- Footer Section Begin -->
<footer class="footer-section1"> 
	<div class="col-lg-12">
		<div class="copyright-text">
			<p> Copyright &copy; PT Perdana Karya Perkasa, Tbk. <script>document.write(new Date().getFullYear());</script>. All Rights Reserved. | <a href="sitemap">Sitemap</a></p> 
		</div>
	</div>
</footer>
<!-- Footer Section End --> 

<!-- Script -->
<?php include 'script.php' ?>
<!-- Script -->