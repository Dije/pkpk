	<!-- Header -->
	<?php $title = "LINGKUNGAN | PKPK";
	$page = "environmental"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/homeController.php');
	$companyprofile = new companyprofileController();
	$home = new homeController();
	$CPdata = $companyprofile->getData();
	$indx = $home->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar --> 

		 <!-- Hero Section Begin --> 
		 <section class="cta-section spad set-bg opacity" data-setbg="../admin/assets/img/esg/environmental-header.png">
			<div class="text">
				<div class="cta-text">
					<h2>Lingkungan</h2> 
					<br>
					<div class="text-sub"> <p style="white-space: pre-line;">PKPK berkomitmen untuk menjaga 
					kelestarian lingkungan demi masa depan berkelanjutan.</p> 
					</div>  
				</div>   
			</div>   
		</section>
		<!-- Hero Section End -->  

		<!-- NEWS Section Begin -->
		<section class="contact-section spad2">
			<div class="container">
				<div class="contact-text">
					<h3><b>Kiat Kami Mewujudkannya</b></h3>  
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							Bergerak di bidang energi, PKPK berkomitmen untuk membantu mewujudkan masa depan berkelanjutan. Hal tersebut bisa terwujud dengan adanya kesadaran akan pentingnya menjaga bumi untuk tetap hijau dan layak. Oleh karena itu, PKPK melakukan tindakan nyata seperti menghemat konsumsi energi listrik, bahan bakar, air, serta material untuk kegiatan operasionalnya.</p> 
						</div>
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							Ditambah dengan gencarnya perubahan iklim yang semakin masif, PKPK berkomitmen untuk terus melakukan deforestasi dan mengembangkan teknologi optimal untuk mengurangi emisi karbon.</p> 
						</div>
					</div>  
					<div class="container">
						<div class="row">
							<!-- Hero Section Begin -->
								<section class="hero-section carouseelll">
									<div class="hs-slider owl-carousel normall"> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/environment/environment-1.png">
										</div>  
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/environment/environment-2.jpg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/environment/environment-3.jpg">
										</div> 
									</div>
								</section>
							<!-- Hero Section End -->
						</div>
					</div>
				</div> 
			</div>
		</section>
		<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
