<?php
    define('PROJECT_ROOT_PATH', __DIR__); 
    include_once (PROJECT_ROOT_PATH . '/../Controller/newsController.php');
    $news = new newsController();
    $perPage = 10;
    $page = 0;

    if (isset($_POST['page']) && isset($_POST['year'])) { 
        $page  = $_POST['page']; 
        $year  = $_POST['year']; 
    } else { 
        $page=1;
        $year="";
    }; 

    $startFrom = ($page-1) * $perPage;    
    if(isset($_POST['year']) && $_POST['year'] != ""){
        $news = $news->getDataLimitbyYear($startFrom, $perPage, $year);  
    }else{ 
        $news = $news->getDataLimit($startFrom, $perPage);  
    } 
     
    $paginationHtml = ''; 
    foreach($news as $dtNews) {
        $bulan = array (1 =>   'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
        $pecahkan = explode('-', date('d-m-Y', strtotime($dtNews['created_date']))); 
        $indoDate = $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0] . ',  ' . $pecahkan[2];
        $paginationHtml.='<tr>';  
        $paginationHtml.='<td style="text-align: center; width:300px"><img class="mx-auto" style="width:200px;height:150px;object-fit: cover;" src="../admin/assets/img/news/'.$dtNews['image'].'"></td>'; 
        $paginationHtml.='<td><b>'.$indoDate.'</b><br /><a>'.$dtNews['news_title_indonesia'].'</td>';
        $paginationHtml.='<td style="text-align: center;"><a class="download" href="./rincian-berita?id='.$dtNews['id'].'">Selengkapnya</a></td>';  
        $paginationHtml.='</tr>';   
    }

    $jsonData = array(
        "html"  => $paginationHtml, 
    );
    
    echo json_encode($jsonData); 
?> 