	<!-- Header -->
	<?php $title = "SOSIAL | PKPK";
	$page = "social"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/homeController.php');
	$companyprofile = new companyprofileController();
	$home = new homeController();
	$CPdata = $companyprofile->getData();
	$indx = $home->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar --> 

		 <!-- Hero Section Begin -->  
		 <section class="cta-section spad set-bg opacity" data-setbg="../admin/assets/img/esg/social-header.png">
			<div class="text">
				<div class="cta-text">
					<h2>Sosial</h2> 
					<br>
					<div class="text-sub"> <p style="white-space: pre-line;">Memberdayakan masyarakat sosial dan karyawan
					menjadi agenda PKPK untuk mewujudkan pilar sosial ESG.</p> 
					</div>  
				</div>   
			</div>   
		</section> 
		<!-- Hero Section End --> 

		<!-- NEWS Section Begin -->
		<section class="contact-section spad2">
			<div class="container">
				<div class="contact-text">
					<h3><b>Kiat Kami Mewujudkannya</b></h3>  
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							PKPK sadar bahwa masyarakat sosial berperan penting dalam perwujudan masa depan berkelanjutan. Sehingga masyarakat perlu dirangkul dan disejahterakan. PKPK terus menggencarkan untuk membuka lapangan kerja dan memberikan bantuan yang menunjang kehidupan masyarakat sekitar.</p> 
						</div>
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							PKPK turut mengutamakan bahwa keselamatan dan kesejahteraan karyawan. Kami selalu memastikan jika seluruh karyawan mendapat perlindungan kerja terbaik. Selain itu, karyawan kami turut mendapatkan kesejahteraan secara materi berupa beberapa benefit yang ditawarkan perusahaan.</p> 
						</div>
					</div>  
					<div class="container">
						<div class="row">
							<!-- Hero Section Begin -->
								<section class="hero-section carouseelll">
									<div class="hs-slider owl-carousel normall"> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-2.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-3.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-4.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-5.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-6.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-7.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-8.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-9.jpeg">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/social/social-10.jpeg">
										</div> 
									</div>
								</section>
							<!-- Hero Section End -->
						</div>
					</div>
				</div> 
			</div>
		</section>
		<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
