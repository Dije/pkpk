<!-- Header -->
<?php $title = "Galeri | PKPK";
$page = "gallery"; 
include 'include/header.php' ?>
<!-- Header -->

<body>
	<!-- Page Preloder -->
	<div id="preloder">
		<div class="loader"></div>
	</div>

	<!-- Navbar -->
	<?php include 'include/navbar.php' ?>
	<!-- Navbar -->

	<!-- Subheader -->
	<?php include 'include/subheader.php' ?>
	<!-- Subheader -->

	<!-- Testimoial Section Begin -->
	<section class="testimonial-section spad2">
		<div class="container">
			<div class="row">  
			<div class="col-lg-5 col-md-5">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px" class="mx-auto zoom" src="../admin/assets/img/esg/social/social-1.png">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px" class="mx-auto zoom" src="../admin/assets/img/esg/social/social-2.jpeg">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px"  class="mx-auto zoom" src="../admin/assets/img/esg/social/social-3.jpeg">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px"  class="mx-auto zoom" src="../admin/assets/img/gallery/gallery-1.jpg">
					<img style="height:300px; width: 500px; object-fit: cover;" class="mx-auto zoom" src="../admin/assets/img/gallery/gallery-2.jpg">
				</div> 
				<div class="col-lg-2 col-md-2">
				</div> 
				<div class="col-lg-5 col-md-5">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px" class="mx-auto zoom" src="../admin/assets/img/esg/social/social-4.jpeg">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px" class="mx-auto zoom" src="../admin/assets/img/esg/social/social-5.jpeg">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px"  class="mx-auto zoom" src="../admin/assets/img/esg/social/social-6.jpeg">
					<img style="height:300px; width: 500px; object-fit: cover; margin-bottom: 10px"  class="mx-auto zoom" src="../admin/assets/img/gallery/gallery-3.jpg">
					<img style="height:300px; width: 500px; object-fit: cover;" class="mx-auto zoom" src="../admin/assets/img/gallery/gallery-4.jpg">
				</div>
			</div>
		</div>
	</section>
	<!-- Testimonial Section End -->

	<!-- Footer -->
	<?php include 'include/footer.php' ?>
	<!-- Footer -->
</body>

</html>