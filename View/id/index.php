<!-- Header -->
<?php $title = "PKPK | Selamat Datang di Perdana Karya Perkasa, Tbk";
$page = "."; 
include 'include/header.php'; 
include_once (PROJECT_ROOT_PATH . '/../../Controller/homeController.php');
include_once (PROJECT_ROOT_PATH . '/../../Controller/newsController.php');
include_once (PROJECT_ROOT_PATH . '/../../Controller/ourBusinessesController.php');
$home = new homeController();
$news = new newsController();
$ourBusinesses = new ourBusinessesController();
$indx = $home->getData();
$newsLatest = $news->getDataLatest();
$newsMain = $news->getDataMain();
$OB = $ourBusinesses->getData();
?>

<!-- Header -->

<body>
  <!-- Page Preloder -->
  <div id="preloder">
    <div class="loader"></div>
  </div>

  <!-- Navbar -->
  <?php include 'include/navbar.php' ?>
  <!-- Navbar -->

  <!-- Hero Section Begin --> 
  <!-- <section class="cta-section spad3 set-bg" data-setbg="admin/assets/img/home/exp2.png">
    <div class="text">
      <div class="cta-text">
        <h2>Perdana Karya Perkasa Tbk (PKPK)</h2> 
        <br>
        <div class="text-sub">
          <p>PT Perdana Karya Perkasa Tbk (PKPK) sendiri merupakan perusahaan bergerak di bidang tambang, dan logistik. </p> 
        </div>  
      </div>   
    </div>  
	</section> -->
  <!-- Hero Section End -->

  <section class="hero-section">
    <div class="hs-slider owl-carousel normal"> 
      <div class="hs-item"> 
        <section class="cta-section spad3 set-bg" data-setbg="../admin/assets/img/home/exp1.png">
          <div class="text" style="margin-right: 60px; margin-bottom: 60px; width:900px; position:absolute; left:0; bottom: 0; margin: 150px 100px">
            <div class="cta-text" style="text-align: left;">
            <h2 style="color:black; white-space: pre-line; text-shadow: none; font-size: 80px;">Moral (<font color="red">德</font>)
              &
              Decency (<font color="red">禮</font>)</h2>  
              <!-- <br>
              <div class="text-sub" style="background-color: transparent">
                <p style="padding-left: 0; color:black; text-shadow: none">PT Perdana Karya Perkasa Tbk (PKPK) sendiri merupakan perusahaan bergerak di bidang tambang, dan logistik. </p> 
              </div>   -->
            </div>   
          </div>  
        </section>
      </div> 
      <!-- <div class="hs-item"> 
        <section class="cta-section spad3 set-bg" data-setbg="../admin/assets/img/home/exp1.png">
          <div class="text" style="margin: 40px 60px; width:800px; position:absolute; left:0">
            <div class="cta-text" style="text-align: left;">
              <h2 style="color:black; text-shadow: none; white-space: pre-line;">Moral (德)
              &
              Decency (禮)</h2>  
               <br>
              <div class="text-sub" style="background-color: transparent">
                <p style="padding-left: 0; color:black; text-shadow: none">PT Perdana Karya Perkasa Tbk (PKPK) sendiri merupakan perusahaan bergerak di bidang tambang, dan logistik. </p> 
              </div>
            </div>   
          </div>  
        </section>
      </div>  -->
      <div class="hs-item"> 
        <section class="cta-section spad3 set-bg" data-setbg="../admin/assets/img/home/exp2.png">
          <div class="text">
            <div class="cta-text">
              <!-- <h2>Perdana Karya Perkasa Tbk (PKPK)</h2>
              <br>
              <div class="text-sub">
                <p>PT Perdana Karya Perkasa Tbk (PKPK) sendiri merupakan perusahaan bergerak di bidang tambang, dan logistik. </p> 
              </div>   --> 
            </div>   
          </div>  
        </section>
      </div> 
      <div class="hs-item"> 
        <section class="cta-section spad3 set-bg" data-setbg="../admin/assets/img/home/exp3.png">
          <div class="text" style="margin: 40px 60px; width:900px; position:absolute; right:0">
            <div class="cta-text" style="text-align: right;">
              <!-- <h2>Perdana Karya Perkasa Tbk (PKPK)</h2> 
              <br>
              <div class="text-sub">
                <p>PT Perdana Karya Perkasa Tbk (PKPK) sendiri merupakan perusahaan bergerak di bidang tambang, dan logistik. </p> 
              </div>   -->
            </div>   
          </div>  
        </section>
      </div>  
      <div class="hs-item"> 
        <section class="cta-section spad3 set-bg" data-setbg="../admin/assets/img/home/exp4.png">
          <div class="text" style="margin-right: 60px; margin-bottom: 60px; width:900px; position:absolute; right:0; bottom: 0;">
            <div class="cta-text" style="text-align: right;">
              <!-- <h2 style="padding-right: 15px; background-color: black;">Perdana Karya Perkasa Tbk (PKPK)</h2> 
              <div class="text-sub" style="margin-top: 10px; background-color: transparent ">
                <p  style="padding-right: 15px; background-color: red;">PT Perdana Karya Perkasa Tbk (PKPK) sendiri merupakan perusahaan bergerak di bidang tambang, dan logistik. </p> 
              </div>   -->
            </div>   
          </div>  
        </section>
      </div> 
  </div>
  </section> 
 <!-- NEWS Section Begin -->
 <section class="contact-section spad2">
		<div class="container">
			<div class="row">
				<div class="col-lg-4 col-md-4">
					<div class="contact-text">
						<h3><b>Berita Terbaru</b></h3> 
						<div class="ct-item"> 
              <?php for($i=0; $i<(count($newsLatest)); $i++){ ?> 
                <div class="ct-text">
                <img style="width:100%;" class="mx-auto" src="../admin/assets/img/news/<?php echo $newsLatest[$i]['image']; ?>">
                  <h5><?php echo $newsLatest[$i]['news_title_indonesia']; ?></h5>
                  <p style="white-space: pre-line; text-align: justify; margin-bottom: 0px;"><?php echo substr($newsLatest[$i]['news_body_indonesia'],0, 100); ?> <a class="see_more" href="./rincian-berita?id=<?php echo $newsLatest[$i]['id']; ?>" target="_blank">Selengkapnya</a></p>
                  <p class="date">
                    <?php $bulan = array (1 =>   'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                          $pecahkan = explode('-', date('d-m-Y', strtotime($newsLatest[$i]['created_date']))); 
                          $indoDate = $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0] . ',  ' . $pecahkan[2];  echo $indoDate; ?>
                  </p>
                  <br>
                </div> 
              <?php } ?>
						</div> 
            <form class="buttonNews" action="./newsroom">
              <button type="submit" class="site-btn">BERITA LAINNYA</button>  
            </form>
					</div>
				</div>
				<div class="col-lg-8 col-md-8">
          <div class="pad20">
					<div class="contact-text news">
						<h3><b>Berita Utama</b></h3> 
              <div class="ct-item">  
                  <div class="ct-text">
                  <img style="width:100%;" class="mx-auto" src="../admin/assets/img/news/<?php echo $newsMain[0]['image']; ?>">
                    <h5><?php echo $newsMain[0]['news_title_indonesia']; ?></h5>
                    <p class="date">
                    <?php $bulan = array (1 =>   'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
                          $pecahkan = explode('-', date('d-m-Y', strtotime($newsMain[0]['created_date']))); 
                          $indoDate = $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0] . ',  ' . $pecahkan[2];  echo $indoDate; ?>
                    </p>
                    <p style="white-space: pre-line; text-align: justify; margin-bottom: 0px;"><?php echo $newsMain[0]['news_body_indonesia']; ?></p>
                  </div>  
              </div> 
              </div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- NEWS Section End --> 
  
  <!-- OUR BUSINESS Section Begin -->
	<section class="contact-section spadBottom">
		<div class="container recontain">
      <h3 style="text-align: center;"><b>BISNIS KAMI</b></h3>  
			<div class="row" style="padding-top:30px;">
				<div class="col-lg-3 col-md-3" style="display: flex;"> 
          <div class="pad20 background"> 
            <div class="image">
              <img class="mx-auto sign image-main" src="../admin/assets/icon/mining-black.png"> 
            </div>
            <div class="contact-text"> 
              <div class="ct-item"> 
                <div class="ct-text">
                  <h5 style="white-space: pre-line;	text-align: center;">TAMBANG</h5> 
                  <button data-toggle="modal" data-target="#MiningModal" style="display:block; margin:auto" type="submit" class="site-btn">RINCIAN</button>  
                </div>
              </div>
            </div>
          </div>
				</div>
				<div class="col-lg-3 col-md-3" style="display: flex;"> 
         <div class="pad20 background"> 
            <div class="image">
              <img class="mx-auto sign image-main" src="../admin/assets/icon/equipment-black.png">
            </div>
            <div class="contact-text"> 
              <div class="ct-item"> 
                <div class="ct-text">
                  <h5 style="white-space: pre-line;	text-align: center;">PERLENGKAPAN</h5> 
                  <button data-toggle="modal" data-target="#EquipmentModal" style="display:block; margin:auto" type="submit" class="site-btn">RINCIAN</button>   
                </div>
              </div>
            </div>
          </div>
				</div> 
        <div class="col-lg-3 col-md-3" style="display: flex;"> 
          <div class="pad20 background"> 
            <div class="image">
              <img class="mx-auto sign image-main" src="../admin/assets/icon/land-preparation-black.png"> 
            </div>
            <div class="contact-text"> 
              <div class="ct-item"> 
                <div class="ct-text">
                  <h5 style="white-space: pre-line;	text-align: center;">PERSIAPAN LAHAN</h5> 
                  <button data-toggle="modal" data-target="#LandPrepModal" style="display:block; margin:auto" type="submit" class="site-btn">RINCIAN</button>   
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-3" style="display: flex;"> 
          <div class="pad20 background">   
            <div class="image">
              <img class="mx-auto sign image-main" src="../admin/assets/icon/construction-black.png"> 
            </div>
            <div class="contact-text"> 
              <div class="ct-item"> 
                <div class="ct-text"> 
                  <h5 style="white-space: pre-line;	text-align: center;">KONSTRUKSI</h5> 
                  <button data-toggle="modal" data-target="#ConstructModal" style="display:block; margin:auto" type="submit" class="site-btn">RINCIAN</button>  
                </div>
              </div>
            </div>
          </div>
        </div>
			</div>  
		</div>
	</section>
	<!-- OUR BUSINESS Section End --> 

  <!-- Footer -->
  <?php include 'include/footer.php' ?>
  <!-- Footer -->
</body>

</html>
 
<!-- Modal -->
<div class="modal fade" id="MiningModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content"> 
      <div class="modal-body">
        <div class="container recontain"> 
            <div class="row">
              <div class="col-lg-4 col-md-4" style="display: flex;"> 
                <div class="pad20 background" style="padding-top: 12px;"> 
                  <div class="image">
                    <img class="mx-auto sign image-main" src="../admin/assets/icon/mining-black.png">
                  </div>
                  <div class="contact-text"> 
                    <div class="ct-item"> 
                      <div class="ct-text">
                        <h5 style="white-space: pre-line;	text-align: center;">TAMBANG</h5>  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-7" style="display:flex;"> 
                <div class="pad20 background">  
                    <div class="contact-text"> 
                      <div class="ct-item"> 
                        <div class="ct-text"> 
                          <p style="white-space: pre-line; text-align: justify;"><?php for($j=0; $j< count($OB); $j++){ ?><?php echo $OB[$j]['tambang']; ?><?php } ?></p>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>  
        </div>
      </div>  
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="EquipmentModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content"> 
      <div class="modal-body">
        <div class="container recontain">  
            <div class="row">
              <div class="col-lg-4 col-md-4" style="display: flex;"> 
                <div class="pad20 background" style="padding-top: 12px;"> 
                  <div class="image">
                    <img class="mx-auto sign image-main" src="../admin/assets/icon/equipment-black.png">  
                  </div>
                  <div class="contact-text"> 
                    <div class="ct-item"> 
                      <div class="ct-text">
                        <h5 style="white-space: pre-line;	text-align: center;">PERLENGKAPAN</h5>  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-7" style="display:flex;"> 
                <div class="pad20 background">  
                    <div class="contact-text"> 
                      <div class="ct-item"> 
                        <div class="ct-text"> 
                        <p style="white-space: pre-line;	text-align: justify;"><?php for($j=0; $j< count($OB); $j++){ ?><?php echo $OB[$j]['perlengkapan']; ?><?php } ?></p>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>  
        </div>
      </div>  
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="LandPrepModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content"> 
      <div class="modal-body">
        <div class="container recontain">  
            <div class="row">
              <div class="col-lg-4 col-md-4" style="display: flex;"> 
                <div class="pad20 background" style="padding-top: 12px;">  
                  <div class="image">
                    <img class="mx-auto sign image-main" src="../admin/assets/icon/land-preparation-black.png"> 
                  </div>
                  <div class="contact-text"> 
                    <div class="ct-item"> 
                      <div class="ct-text">
                        <h5 style="white-space: pre-line;	text-align: center;">PERSIAPAN LAHAN</h5>  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-7" style="display:flex;"> 
                <div class="pad20 background">  
                    <div class="contact-text"> 
                      <div class="ct-item"> 
                        <div class="ct-text"> 
                        <p style="white-space: pre-line;	text-align: justify;"><?php for($j=0; $j< count($OB); $j++){ ?><?php echo $OB[$j]['lahan']; ?><?php } ?></p>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>  
        </div>
      </div>  
    </div>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="ConstructModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content"> 
      <div class="modal-body">
        <div class="container recontain"> 
            <div class="row">
              <div class="col-lg-4 col-md-4" style="display:flex;"> 
                <div class="pad20 background" style="padding-top: 12px;"> 
                  <div class="image">
                    <img class="mx-auto sign image-main" src="../admin/assets/icon/construction-black.png"> 
                  </div>
                  <div class="contact-text"> 
                    <div class="ct-item"> 
                      <div class="ct-text">
                        <h5 style="white-space: pre-line;	text-align: center;">KONSTRUKSI</h5>  
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-lg-7 col-md-7" style="display:flex;"> 
                <div class="pad20 background">  
                    <div class="contact-text"> 
                      <div class="ct-item"> 
                        <div class="ct-text"> 
                          <p style="white-space: pre-line; text-align: justify;"><?php for($j=0; $j< count($OB); $j++){ ?><?php echo $OB[$j]['konstruksi']; ?><?php } ?></p>
                        </div>
                      </div>
                    </div>
                </div>
              </div>
            </div>  
        </div>
      </div>  
    </div>
  </div>
</div>