	<!-- Header -->
	<?php $title = "Berita | PKPK"; 
	$id = $_GET['id'];
	$page = "newsroom-detail?id=".$id;  
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/newsController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/subheaderController.php'); 
	$companyprofile = new companyprofileController();
	$news = new newsController(); 
	$subhead = new subheaderController();
	$CPdata = $companyprofile->getData();
	$selectNews = $news->getDataByUid($id);
	$allNews = $news->getDataExcUid($id);
	$sheadPage = $subhead->getDataByPageEnglish('newsroom-detail');
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<!-- <div id="preloder">
			<div class="loader"></div>
		</div> -->

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar -->

		<!-- Subheader -->
		<?php   
		for($i=0; $i< count($sheadPage); $i++){ ?>
			<!-- Cta Section Begin -->
			<section class="cta-section spad set-bg" data-setbg="../admin/assets/img/subheader/<?php echo $sheadPage[$i]['sub_header']; ?>">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="cta-text">
								<!-- <h2><?php echo $sheadPage[$i]['PageNameInd']; ?></h2> -->
								<p> <br /><br /> <br />  </p>
							</div>
						</div>
					</div>
				</div>
			</section>
			<!-- Cta Section End -->
		<?php } ?>
		<!-- Subheader -->

		<!-- NEWS Section Begin -->
	<section class="contact-section spad2">
		<div class="container">
			<div class="row">
			<div class="col-lg-8 col-md-8">
					<div class="contact-text"> 
						<div class="ct-item"> 
							<div class="ct-text">
								<h5 style="white-space: pre-line; text-align: justify;"><?php echo $selectNews[0]['news_title_indonesia']?></h5>
								<p class="date">
									<?php $bulan = array (1 =>   'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
										$pecahkan = explode('-', date('d-m-Y', strtotime($selectNews[0]['created_date']))); 
										$indoDate = $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0] . ',  ' . $pecahkan[2];  echo $indoDate; ?>
								</p>
								<img style="width:100%;"  class="mx-auto" src="../admin/assets/img/news/<?php echo $selectNews[0]['image']; ?>"> 
								<p style="white-space: pre-line; text-align: justify;"><?php echo $selectNews[0]['news_body_indonesia']?></p>
							</div>
						</div>
					</div>
			</div>
			<div class="col-lg-4 col-md-4">
				<div class="pad20">
					<div class="contact-text news"> 
						<div class="ct-item"> 
							<?php for($i=0; $i<(count($allNews)); $i++){ ?> 
								<div class="ct-text">
								<img style="width:100%;" class="mx-auto" src="../admin/assets/img/news/<?php echo $allNews[$i]['image']; ?>">
								<h5><?php echo $allNews[$i]['news_title_indonesia']; ?></h5>
								<p style="white-space: pre-line; text-align: justify; margin-bottom: 0px;"><?php echo substr($allNews[$i]['news_body_indonesia'],0, 100); ?> <a class="see_more" href="./rincian-berita?id=<?php echo $allNews[$i]['id']; ?>" target="_blank">Selengkapnya</a></p>
								<p class="date">
									<?php $bulan = array (1 =>   'Januari','Februari','Maret','April','Mei','Juni','Juli','Agustus','September','Oktober','November','Desember');
										$pecahkan = explode('-', date('d-m-Y', strtotime($allNews[$i]['created_date']))); 
										$indoDate = $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0] . ',  ' . $pecahkan[2];  echo $indoDate; ?>
								</p>
								<br>
								</div> 
							<?php } ?>
						</div>
					</div> 
				</div>  
			</div>  
		</div> 
	</div> 
</section>
<!-- NEWS Section End --> 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
