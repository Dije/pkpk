	<!-- Header -->
	<?php $title = "ESG | PKPK";
	$page = "esg"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/companyprofileController.php'); 
	$companyprofile = new companyprofileController();
	$CPdata = $companyprofile->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar --> 

		<!-- Hero Section Begin --> 
		<section class="cta-section spad set-bg opacity" data-setbg="../admin/assets/img/esg/header.png">
			<div class="text">
				<div class="cta-text">
					<h2>Lingkungan,
						Sosial,
						& Tata Kelola
					</h2> 
					<br>
					<div class="text-sub">
						<p>Perwujudan ESG yang nyata adalah komitmen jangka panjang kami demi masa depan berkelanjutan. Ketiga prinsip ESG menjadi kiat kami untuk berjuang setiap harinya. </p> 
					</div>  
				</div>   
			</div>  
		</section>
		<!-- Hero Section End --> 

		<!-- NEWS Section Begin -->
	<section class="contact-section spad2">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8">
					<div class="contact-text">
						<h3><b>Kiat kami mewujudkan ESG</b></h3>  
						<p style="white-space: pre-line; text-align: justify;">
						PKPK berpegang teguh bahwa untuk mewujudkan masa depan berkelanjutan dibutuhkan keselarasan antara pemeliharaan lingkungan, andil dalam kehidupan sosial untuk menyejahterakan masyarakat, dan tata kelola perusahaan yang transparan. </p> 
					</div>
				</div>  
				<div class="container">
					<div class="row">
						<div class="col-lg-4 col-md-4">
							<div class="contact-text esgcontainer">
								<img class="mx-auto esg" src="../admin/assets/img/esg/environmental.png"> 
								<div class="bottom-left">
									<img class="mx-auto esg-icon" src="../admin/assets/icon/esg/environment-icon.png"> 
									<h6>LINGKUNGAN</h6>   
									<p class="esgheight">
									Bergerak di bidang energi, PKPK berkomitmen untuk membantu mewujudkan masa depan berkelanjutan.</p>
									<p class="see-more"><a href="./lingkungan">Selengkapnya</a></p>
								</div>  
							</div>
						</div>  
						<div class="col-lg-4 col-md-4 pad20">
							<div class="contact-text esgcontainer">
								<img class="mx-auto esg" src="../admin/assets/img/esg/social.png"> 
								<div class="bottom-left"> 
									<img class="mx-auto esg-icon" src="../admin/assets/icon/esg/social-icon.png"> 
									<h6>SOSIAL</h6>   
									<p class="esgheight">
									PKPK bertekad mewujudkan kesejahteraan masyarakat dan tenaga kerja karena keduanya berperan penting demi masa depan berkelanjutan.</p>
									<p class="see-more"><a href="./sosial">Selengkapnya</a></p>
								</div>   
							</div>
						</div>  
						<div class="col-lg-4 col-md-4 pad20">
							<div class="contact-text esgcontainer">
								<img class="mx-auto esg" src="../admin/assets/img/esg/governance.png"> 
								<div class="bottom-left"> 
									<img class="mx-auto esg-icon" src="../admin/assets/icon/esg/governance-icon.png"> 
									<h6>TATA KELOLA</h6>  
									<p class="esgheight">
									Kami bertekad menjalankan perusahaan dengan tata kelola yang baik dengan aspek GCG (Good Corporate Governance).</p>
									<p class="see-more"><a href="./tata-kelola">Selengkapnya</a></p>
								</div>  
							</div>
						</div>  
					</div>
				</div> 
			</div> 
		</div>
	</section>
	<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
