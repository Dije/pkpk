<?php
    define('PROJECT_ROOT_PATH', __DIR__); 
    include_once (PROJECT_ROOT_PATH . '/../Controller/newsController.php');
    $news = new newsController();

    if(isset($_POST['year']) && $_POST['year'] != ""){
        $data = $news->getDatabyYear($_POST['year']);  
    }else{ 
        $data = $news->getData();  
    } 


    $perPage = 10;  
    $totalRecords = count($data);
    $totalPages = ceil($totalRecords/$perPage);

    $jsonData = array(
        "totalData"  => $totalPages, 
    );
    
    echo json_encode($jsonData); 
?> 