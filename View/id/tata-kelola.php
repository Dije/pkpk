	<!-- Header -->
	<?php $title = "TATA KELOLA | PKPK";
	$page = "governance"; 
	include 'include/header.php'; 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/companyprofileController.php'); 
	include_once (PROJECT_ROOT_PATH . '/../../Controller/homeController.php');
	$companyprofile = new companyprofileController();
	$home = new homeController();
	$CPdata = $companyprofile->getData();
	$indx = $home->getData();
	?>
	<!-- Header -->

	<body>
		<!-- Page Preloder -->
		<div id="preloder">
			<div class="loader"></div>
		</div>

		<!-- Navbar -->
		<?php include 'include/navbar.php' ?>
		<!-- Navbar --> 

		<!-- Hero Section Begin --> 
		<section class="cta-section spad set-bg opacity" data-setbg="../admin/assets/img/esg/governance-header.png">
			<div class="text">
				<div class="cta-text">
					<h2>TATA KELOLA</h2> 
					<br>
					<div class="text-sub"> <p style="white-space: pre-line;">Kami bertekad untuk membangun tata kelola perusahaan 
					yang selaras dengan aspek GCG (Good Corporate Governance).</p> 
					</div>  
				</div>   
			</div>   
		</section>
		<!-- Hero Section End -->  

		<!-- NEWS Section Begin -->
		<section class="contact-section spad2">
			<div class="container">
				<div class="contact-text">
					<h3><b>Kiat Kami Mewujudkannya</b></h3>  
					<div class="row">
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							Kami bertekad menciptakan tata kelola perusahaan yang selaras dengan aspek GCG (Good Corporate Governance). Aspek tersebut meliputi transparansi, akuntabilitas, pertanggung jawaban, kemandirian, dan keadilan.</p> 
						</div>
						<div class="col-lg-6 col-md-6">
							<p style="white-space: pre-line; text-align: justify;">
							Oleh karena itu, sambil menerapkan tata kelola yang beretika seperti di atas, PKPK berkomitmen untuk terus membangun bisnis inovatif dan bersih.</p> 
						</div>
					</div>   
					<div class="container">
						<div class="row">
							<!-- Hero Section Begin -->
								<section class="hero-section carouseelll">
									<div class="hs-slider owl-carousel normall"> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/governance/governance-1.png">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/governance/governance-2.JPG">
										</div> 
										<div class="hs-item"> 
											<img class="mx-auto carousel" src="../admin/assets/img/esg/governance/governance-3.png">
										</div> 
									</div>
								</section>
							<!-- Hero Section End -->
						</div>
					</div>
				</div> 
			</div>
		</section>
		<!-- NEWS Section End --> 
	 

		<!-- Footer -->
		<?php include 'include/footer.php' ?>
		<!-- Footer -->
	</body>

	</html>
