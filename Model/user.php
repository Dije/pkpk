<?php 
class user
{
    private $id_user;
    private $username;
    private $firstname;
    private $lastname;
    private $password;
    private $emailaddress;
    private $phonenumber;
    public function __construct() {}
    // public function __construct($id_user, $firstname, $lastname, $password, $emailaddress, $phonenumber)
    // {
    //     $this->id_user = $id_user;
    //     $this->firstname   = $firstname;
    //     $this->lastname   = $lastname;
    //     $this->password   = $password;
    //     $this->emailaddress   = $emailaddress;
    //     $this->phonenumber   = $phonenumber;
    // }
    public function get_id_user()
    {
        return $this->id_user;
    }
    public function set_id_role($id_user)
    {
        $this->id_user = $id_user;
    }
    public function get_username()
    {
        return $this->username;
    }
    public function set_username($username)
    {
        $this->username = $username;
    }
    public function get_firstname()
    {
        return $this->firstname;
    }
    public function set_firstname($firstname)
    {
        $this->firstname = $firstname;
    }
    public function get_lastname()
    {
        return $this->lastname;
    }
    public function set_lastname($lastname)
    {
        $this->lastname = $lastname;
    }
    public function get_password()
    {
        return $this->password;
    }
    public function set_password($password)
    {
        $this->password = $password;
    }
    public function get_emailaddress()
    {
        return $this->emailaddress;
    }
    public function set_emailaddress($emailaddress)
    {
        $this->emailaddress = $emailaddress;
    }
    public function get_phonenumber()
    {
        return $this->phonenumber;
    }
    public function set_phonenumber($phonenumber)
    {
        $this->phonenumber = $phonenumber;
    }
}
?>